/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include "virusTest.c"
extern JNIEnv *javaEnv;

JNIEXPORT void JNICALL Java_virusTest_testLookForAllCell
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testLookForAllCell();
}

JNIEXPORT void JNICALL Java_virusTest_testLookForAllCellBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testLookForAllCellBis();
}

JNIEXPORT void JNICALL Java_virusTest_testLookForAllCellBisBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testLookForAllCellBisBis();
}

JNIEXPORT void JNICALL Java_virusTest_testLookForCell
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testLookForCell();
}

JNIEXPORT void JNICALL Java_virusTest_testMove
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testMove();
}

JNIEXPORT void JNICALL Java_virusTest_testMoveAtEdge
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testMoveAtEdge();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnLeft
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnLeft180
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft180();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnLeft270
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft270();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnLeft90
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft90();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnLeftAtEdge
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeftAtEdge();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnRight
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnRight180
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight180();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnRight270
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight270();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnRight90
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight90();
}

JNIEXPORT void JNICALL Java_virusTest_testTurnRightAtEdge
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRightAtEdge();
}

JNIEXPORT void JNICALL Java_virusTest_testTwoMoveAtEdge
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTwoMoveAtEdge();
}

