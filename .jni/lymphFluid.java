/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class lymphFluid extends greenfoot.World {

	public lymphFluid() {
    	super(width(), height(), cellSize());
		start();
	}
    
    public void start(){
        start_();
    }
    private native void start_();
    
    private native static int width();
    private native static int height();
    private native static int cellSize();


    static {
        System.load(new java.io.File(".jni", "lymphFluid_jni.so").getAbsolutePath());
    }
}
