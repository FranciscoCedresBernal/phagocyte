/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

#include "virus.c"

JNIEXPORT void JNICALL Java_virus_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startVirus(object);
}

JNIEXPORT void JNICALL Java_virus_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actVirus(object);
}

