/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;
@RunWith(GreenfootRunner.class)
public class cellTest {
    @Test
    public native void testNoMove();

    static {
        System.load(new java.io.File(".jni", "cellTest_jni.so").getAbsolutePath());
    }
}
