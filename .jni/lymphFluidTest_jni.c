/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include "lymphFluidTest.c"
extern JNIEnv *javaEnv;

JNIEXPORT void JNICALL Java_lymphFluidTest_testStartGame
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testStartGame();
}

