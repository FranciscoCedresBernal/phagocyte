/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include "cellTest.c"
extern JNIEnv *javaEnv;

JNIEXPORT void JNICALL Java_cellTest_testNoMove
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testNoMove();
}

