/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Define the world size: width, height, and cell size
 */
#define _width    560
#define _height   560
#define _cellSize 1

/**
 * Initialize the background and actors when the game starts:
 * one phagocyte, three viruss, and ten cells.
 */
void startLymphFluid(World lymphFluid) {
	setBackgroundFile(lymphFluid, "lymphFluid.jpg");

	Actor phagocyte = newActor("phagocyte");
    addActorToWorld(lymphFluid, phagocyte, 231, 203);

    Actor virus1 = newActor("virus");
    addActorToWorld(lymphFluid, virus1, 334, 65);
    Actor virus2 = newActor("virus");
    addActorToWorld(lymphFluid, virus2, 481, 481);
    Actor virus3 = newActor("virus");
    addActorToWorld(lymphFluid, virus3, 79, 270);

    Actor cell1  = newActor("cell");
    addActorToWorld(lymphFluid, cell1, 445, 137);
    Actor cell2 = newActor("cell");
    addActorToWorld(lymphFluid, cell2, 454, 369);
    Actor cell3 = newActor("cell");
    addActorToWorld(lymphFluid, cell3, 368, 466);
    Actor cell4 = newActor("cell");
    addActorToWorld(lymphFluid, cell4, 129, 488);
    Actor cell5 = newActor("cell");
    addActorToWorld(lymphFluid, cell5, 254, 388);
    Actor cell6 = newActor("cell");
    addActorToWorld(lymphFluid, cell6, 106, 334);
    Actor cell7 = newActor("cell");
    addActorToWorld(lymphFluid, cell7, 338, 112);
    Actor cell8 = newActor("cell");
    addActorToWorld(lymphFluid, cell8, 150, 94);
    Actor cell9 = newActor("cell");
    addActorToWorld(lymphFluid, cell9, 373, 240);
    Actor cell10 = newActor("cell");
    addActorToWorld(lymphFluid, cell10, 509, 55);
}

