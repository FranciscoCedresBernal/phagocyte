/**
 * This file defines a phagocyte.
 *
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image and the counters virusEaten and delayEaten.
 */
void startPhagocyte(Actor phagocyte) {
	setImageFile(phagocyte, "phagocyte.png");
	setGlobalByte(phagocyte, "virusEaten", 0);
	setGlobalByte(phagocyte, "delayEaten", 0);
}

/**
 * Check if a keyboard control key has been pressed. If so,
 * react accordingly.
 */
void checkKeypress(Actor phagocyte) {
	if (isKeyDown("left")) {
		turn(phagocyte, -4);
	}
	if (isKeyDown("right")) {
		turn(phagocyte, 4);
	}
}

/**
 * Check if we have encountered a virus. If so, eat it. If not,
 * it does nothing. When there are no bacteria left, we win.
 */
void lookForVirus(Actor phagocyte) {
	if (isTouching(phagocyte, "virus")) {
		removeTouching(phagocyte, "virus");
		playSound("au.wav");
		setImageFile(phagocyte, "phagocyte-1.png");
		int virusEaten = getGlobalByte(phagocyte, "virusEaten") + 1;
		setGlobalByte(phagocyte, "virusEaten", virusEaten);
	}
}

/**
 * After eating, the image of the phagocyte changes, going through
 * the sequence image-1, image-2, image-3 and finally image
 */
void switchImage(Actor phagocyte) {
	if (strcmp(getImageFile(phagocyte),"phagocyte-1.png") == 0) {
		int delayEaten = getGlobalByte(phagocyte, "delayEaten");
		if (delayEaten == 3) {
			setImageFile(phagocyte, "phagocyte-2.png");
			setGlobalByte(phagocyte, "delayEaten", 0);
		} else {
			setGlobalByte(phagocyte, "delayEaten", delayEaten+1);
		}
	} else 	if (strcmp(getImageFile(phagocyte),"phagocyte-2.png") == 0) {
		int delayEaten = getGlobalByte(phagocyte, "delayEaten");
		if (delayEaten == 3) {
			setImageFile(phagocyte, "phagocyte-3.png");
			setGlobalByte(phagocyte, "delayEaten", 0);
		} else {
			setGlobalByte(phagocyte, "delayEaten", delayEaten+1);
		}
	} else 	if (strcmp(getImageFile(phagocyte),"phagocyte-3.png") == 0) {
		int delayEaten = getGlobalByte(phagocyte, "delayEaten");
		if (delayEaten == 3) {
			setImageFile(phagocyte, "phagocyte.png");
			setGlobalByte(phagocyte, "delayEaten", 0);
			if (getGlobalByte(phagocyte, "virusEaten") == 3) {
				playSound("fanfare.wav");
				stopScenario();
			}
		} else {
			setGlobalByte(phagocyte, "delayEaten", delayEaten+1);
		}
	}
}

/**
 * Act - do whatever the phagocyte wants to do. This function
 * is called whenever the 'Act' or 'Run' button gets pressed
 * in the environment.
 */
void actPhagocyte(Actor phagocyte) {
	if (strcmp(getImageFile(phagocyte),"phagocyte.png") == 0) {
	    checkKeypress(phagocyte);
		move(phagocyte, 5);
		lookForVirus(phagocyte);
	} else {
		switchImage(phagocyte);
	}
}
